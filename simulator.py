from selenium import webdriver
import pyautogui

# from PIL import Image
# from io import BytesIO


class Simulator(object):
    """
    A class used to represent a Human Interaction Simulation

    ...

    Attributes
    ----------
    driver : str
        path to browser driver
    url : str
        url with form

    Methods
    -------
    screenshot()
        creates screenshot of web-page and returns path to screenshot
    move(x=None, y=None)
        moves mouse to point
    click()
        mouse click on current position
    write(text=None)
        type text through keyboard
    close()
        closes browser
    """


    def __init__(self, driver: str, url: str):
        """
        Parameters
        driver : str
            path to browser driver
        url : str
            url with form
        """

        self.screen_width, self.screen_height = pyautogui.size()
        self.browser = webdriver.Firefox(executable_path=driver)
        # self.browser.set_window_size(int(self.screen_width*0.7), self.screen_height)
        self.browser.maximize_window()
        self.browser.get(url)

        self.firefox_y_difference = 72
        browser_coords = self.browser.get_window_position()
        self.page_x = browser_coords.get("x")
        self.page_y = browser_coords.get("y") + self.firefox_y_difference
        
        self.mouse_move_time = 0.5
        # self.mouse_x, self.mouse_y = pyautogui.position()

        


    def screenshot(self):
        """Creates screenshot of web-page and returns path to screenshot"""
        self.browser.get_screenshot_as_file('./screenshot.png')
        return "screenshot.png"

    def move(self, x: int, y: int):
        """Moves mouse to point

        Parameters
        ----------
        x: int
            gorizontal coordinate
        y: int
            vertical coordinate
        """
        destination_x = self.page_x + x
        destination_y = self.page_y + y
        pyautogui.moveTo(destination_x, destination_y, self.mouse_move_time, pyautogui.easeInQuad)
        

    def click(self):
        """Click mouse on current position"""
        pyautogui.click()


    def write(self, text: str):
        """Type text through keyboard

        Parameters
        ----------
        text: str
            text to type on current position
        """
        pyautogui.typewrite(text, interval=0.02)
    
    def close(self):
        """Closes browser"""
        self.browser.close()