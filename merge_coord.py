import json
import time
import cv2
import os
import re
import numpy as np
import Levenshtein
import pytesseract as ts

class Mapping:
    def __init__(self, json_path, image_path, cases_path):
        self.json_path = json_path
        self.image_path = image_path
        self.cases_path = cases_path
        self.PATH = os.getcwd()
        self.label_coords = []
        self.input_dropbox_coords = []
        self.button_coords = []
        self.distance = []
        self.correct_labels = []
        
        # Lists to classify label and field
        self.security_code = ['cvv2', 'cvc2/cvv', 'cvc/cvv', 'security code', 'security code (cvc)', 'card code', 'card code (cvc)', 'cvc-code']
        self.card_number = ['card number', 'номер картки', 'номер карти', 'номер карты']
        self.name = ['name on card', 'card holder', 'card holder name', 'имя на карте', 'владелец карты', 'имя носителя', 'носитель карты', 'имя носителя карты']
        self.date = ['date', 'expiry date', 'expiration date', 'срок действия', 'срок истечения', 'термін дії']
        self.month = ['month', 'месяц', 'місяць']
        self.year = ['year', 'год', 'рік']
        self.type = ['card type', 'type']
        self.email = ['email', 'e-mail']
        self.phone = ['phone number', 'номер телефона', 'номер телефону', 'phone', 'telephone']
        self.username = ['username', 'username or email']
        self.password = ['password']
        
        # Create folders for temporary image storing
        if not os.path.exists('images'):
            os.mkdir('images')
        else:
            None
            
        if not os.path.exists('cropper'):
            os.mkdir('cropper')
        else:
            None 
            

    # Calculation of Euclidean distance   
    def __euclidean(self, center1, center2, top, l_top, bot, l_bot, f_type):
        return sum((top_i - l_top_i)**2 for top_i, l_top_i in zip(top, l_top))**0.5, locals()
    #(sum((center1_i - center2_i)**2 for center1_i, center2_i in zip(center1, center2))+
    
    
    # Autocomplete words using Levenstein distance
    def __Levenstein_check(self, ocrd_label, case):
        ocrd_label_correct = ''
        if Levenshtein.distance(ocrd_label, case) <= 2 and len(ocrd_label) != Levenshtein.distance(ocrd_label, case)-2:
            edit = Levenshtein.editops(ocrd_label, case)
            ocrd_label_correct = Levenshtein.apply_edit(edit, ocrd_label, case)
        elif Levenshtein.distance(ocrd_label, case) == 0:
            ocrd_label_correct = ocrd_label
        return ocrd_label_correct

    # Get all coordinates of particular class
    def __extractor(self):
        try:
            with open (self.json_path, 'r') as file:
                data = json.load(file)
   
                lb_data = data[0]['labels']
                inp_data = data[0]['inputs']
                drbx_data = data[0]['dropboxes']
                btns_data = data[0]['buttons']
    
                for cntr_l in lb_data:
                    self.label_coords.append([cntr_l['center'], [cntr_l['x1'], cntr_l['y1']], [cntr_l['x2'], cntr_l['y2']]])
                for cntr_i in inp_data: 
                    self.input_dropbox_coords.append([cntr_i['center'], [cntr_i['x1'], cntr_i['y1']], [cntr_i['x2'], cntr_i['y2']], 'input'])
                for cntr_d in drbx_data:
                    self.input_dropbox_coords.append([cntr_d['center'], [cntr_d['x1'], cntr_d['y1']], [cntr_d['x2'], cntr_d['y2']], 'dropbox'])
                for cntr_b in btns_data:
                    self.button_coords.append(([cntr_b['center'], [cntr_b['x1'], cntr_b['y1']], [cntr_b['x2'], cntr_b['y2']], 'button']))
        except FileNotFoundError:
            print('JSON path is incorrect')
            
            
    # Visualize predictions        
    def draw_boxes(self, img_path, button_coords, label_coords, input_dropbox_coords):
        try:
            os.chdir('images')
            
            for counter, b in enumerate(button_coords):
                img = cv2.imread(img_path)
                lineThickness = 2
                butt = cv2.rectangle(img, (int(b[1][0]), int(b[1][1])), (int(b[2][0]), int(b[2][1])), (0,0,255), lineThickness)
                cv2.imwrite("button" + str(counter)+ ".jpg", butt)
    
            try:
                for counter_l, l in enumerate(label_coords):
                    img = cv2.imread(img_path)
                    lineThickness = 2
                    rec = cv2.rectangle(img, (int(l[1][0]), int(l[1][1])), (int(l[2][0]), int(l[2][1])), (255,255,0), lineThickness)
                    cv2.imwrite("kad" + str(counter_l) + ".jpg", rec)
            except Exception:
                print('No labels found')
                
            for counter_i_d, i in enumerate(input_dropbox_coords):
                img = cv2.imread(img_path)
                lineThickness = 2
                rec = cv2.rectangle(img, (int(i[1][0]), int(i[1][1])), (int(i[2][0]), int(i[2][1])), (255,0,0), lineThickness)
                cv2.imwrite("kvad" + str(counter_i_d) + ".jpg", rec)
                
            os.chdir(self.PATH)
            
        except FileNotFoundError:
            print('Image path is incorrect')
            
            
    # Clearly        
    def search_dependency(self, img_path, input_dropbox_coords, label_coords):
        tmp = []
        for num,i in enumerate(input_dropbox_coords):
            for count,l in enumerate(label_coords):
                dst = self.__euclidean(i[0], l[0], i[1], l[1], i[2], l[2], i[3])
                tmp.append(dst)
                
                #Draw black lines to labels 
                img = cv2.imread(img_path)
                lineThickness = 2
                lined = cv2.line(img, (int(i[0][0]), int(i[0][1])), (int(l[0][0]), int(l[0][1])), (0,0,0), lineThickness)
                cv2.imwrite("lined" + str(num) + '_' + str(count) + ".jpg", lined)
                
            # Store minimum distance, draw shortest lines and clear temporary list
            tmp.sort()
            self.distance.append(min(tmp))
            img_buf = cv2.imread(img_path)
            lined_shortest = cv2.line(img_buf, (int(min(tmp)[1]['center1'][0]), int(min(tmp)[1]['center1'][1])),
                                               (int(min(tmp)[1]['center2'][0]), int(min(tmp)[1]['center2'][1])),
                                               (0,255,0), lineThickness)
            rec_i_d = cv2.rectangle(img_buf, (int(min(tmp)[1]['top'][0]), int(min(tmp)[1]['top'][1])), (int(min(tmp)[1]['bot'][0]), int(min(tmp)[1]['bot'][1])), (100,255,100), lineThickness)
            rec_l = cv2.rectangle(img_buf, (int(min(tmp)[1]['l_top'][0]), int(min(tmp)[1]['l_top'][1])), (int(min(tmp)[1]['l_bot'][0]), int(min(tmp)[1]['l_bot'][1])), (100,255,100), lineThickness)
            cv2.imwrite("lined" + str(num) + '_' + str(count+1) + ".jpg", lined_shortest)
            tmp.clear()
                  
            
    def cropper(self, label_coords):
        for count, lb in enumerate(label_coords):
            image = cv2.imread(self.image_path)
            cropped = image[int(lb[1][1]):int(lb[2][1]), int(lb[1][0])-10:int(lb[2][0])]
            scale_percent = 220
            width = int(cropped.shape[1] * scale_percent / 100)
            height = int(cropped.shape[0] * scale_percent / 100)
            dim = (width, height)
            resized = cv2.resize(cropped, dim, interpolation = cv2.INTER_AREA)
            brightness = 0
            contrast = 10
            out = np.int16(resized)
            out = out * (resized / 127 + 1) - contrast + brightness
            out = np.clip(resized, 0, 255)
            out = np.uint8(resized)
            name = str(lb[0][1]) + str(count) + '.jpg'
            cv2.imwrite(name ,out)
            
        
    def get_labels_meaning(self, cases_path):
        labels = []
        cases = []
        
        file = open(cases_path, 'r')
        for case in file:
            case = case.rstrip()
            cases.append(case)
        
        extensions = ('.jpg', '.png')
        images = [img for img in sorted(os.listdir(os.getcwd())) if img.endswith(extensions)]

        
        for image in images:
            ocrd_label_coord = float(image.strip('.jpg'))
            ocrd_label = ts.image_to_string(image, lang='eng')
            print('Initial:', '\n', ocrd_label)
            ocrd_label = re.sub(r'[^-/()\w\s]', '', ocrd_label)
            ocrd_label = ocrd_label.rstrip()
            ocrd_label = ocrd_label.lower()
            print('Postprocessed:', '\n', ocrd_label)
                
            if ocrd_label in cases:
                self.correct_labels.append([ocrd_label, ocrd_label_coord])
            else:    
                labels.append([ocrd_label, ocrd_label_coord])
                
        # print(labels)
        
        for label in labels:
            for case in cases:
                if self.__Levenstein_check(label[0], case):
                    label[0] = self.__Levenstein_check(label[0], case)
                    self.correct_labels.append(label)
        
    
    def __visualizer(self):
        image_folder = 'images'
        video_name = 'Visualizer.avi'

        extensions = ('.jpg', '.png')

        images = [img for img in os.listdir(image_folder) if img.endswith(extensions)]
        images.sort()
        frame = cv2.imread(os.path.join(image_folder, images[0]))
        height, width, layers = frame.shape

        video = cv2.VideoWriter(video_name, 0, 1, (width,height))

        for image in images:
            video.write(cv2.imread(os.path.join(image_folder, image)))

        cv2.destroyAllWindows()
        video.release()
        
    
    def classifier(self):
        # Get data from predicted_coords.json
        self.__extractor()
        # Visualize
        self.draw_boxes(self.image_path, self.button_coords, self.label_coords, self.input_dropbox_coords)
        
        # Search of couple label-field
        final_list = []
        if len(self.label_coords)>=2:
            os.chdir('images')
            self.search_dependency(self.image_path, self.input_dropbox_coords, self.label_coords)
            os.chdir(self.PATH)

            # Crop label, OCR and classify previously found couple
            os.chdir('cropper')
            self.cropper(self.label_coords)

            # Determine label class (v0.1 TODO: impove classification)
            self.get_labels_meaning(self.cases_path)
            print(self.correct_labels)

            for counter, label_name in enumerate(self.correct_labels):
                matches = []
                if label_name[0] in self.card_number:
                    cls = 'Card number'
                    label_name.append(cls)
                elif label_name[0] in self.date:
                    cls = 'Date'
                    label_name.append(cls)
                elif label_name[0] in self.month:
                    cls = 'Month'
                    label_name.append(cls)
                elif label_name[0] in self.year:
                    cls = 'Year'
                    label_name.append(cls)
                elif label_name[0] in self.security_code:
                    cls = 'Security code'
                    label_name.append(cls)
                elif label_name[0] in self.type:
                    cls = 'Card type'
                    label_name.append(cls)
                elif label_name[0] in self.name:
                    cls = 'Name on card'
                    label_name.append(cls)
                elif label_name[0] in self.email:
                    cls = 'Email'
                    label_name.append(cls)
                elif label_name[0] in self.phone:
                    cls = 'Phone number'
                    label_name.append(cls)
                elif label_name[0] in self.username:
                    cls = 'Username'
                    label_name.append(cls)
                elif label_name[0] in self.password:
                    cls = 'Password'
                    label_name.append(cls)
                else:
                    print('Bad luck, buddy')

             
            for counter_d, dist in enumerate(self.distance):
                for label_name in self.correct_labels:
                    if round(label_name[1], 3) == round(dist[1]['center2'][1], 3):
                        coords_names = ['label_top', 'label_bot', 'top', 'bot', 'field_type']
                        coordinates = dict(zip(coords_names, [dist[1]['l_top'], dist[1]['l_bot'], dist[1]['top'], dist[1]['bot'], dist[1]['f_type']]))
                        final_list.append([label_name[2], coordinates])
        elif not self.label_coords or len(self.label_coords)<len(self.input_dropbox_coords):
            for idx, inp_place in enumerate(self.input_dropbox_coords[:-1]):
                if inp_place[1][1] < self.input_dropbox_coords[idx+1][1][1]:
                    params = ['top', 'bot']
                    place = dict(zip(params, [[inp_place[1][0], inp_place[1][1]], [inp_place[2][0], inp_place[2][1]]]))
                    final_list.append(["Username", place])
                    place = dict(zip(params, [[self.input_dropbox_coords[idx+1][1][0], self.input_dropbox_coords[idx+1][1][1]], [self.input_dropbox_coords[idx+1][2][0], self.input_dropbox_coords[idx+1][2][1]]]))
                    final_list.append(["Password", place])
                else:
                    params = ['top', 'bot']
                    place = dict(zip(params, [[inp_place[1][0], inp_place[1][1]], [inp_place[2][0], inp_place[2][1]]]))
                    final_list.append(["Password", place])
                    place = dict(zip(params, [[self.input_dropbox_coords[idx+1][1][0], self.input_dropbox_coords[idx+1][1][1]], [self.input_dropbox_coords[idx+1][2][0], self.input_dropbox_coords[idx+1][2][1]]]))
                    final_list.append(["Username", place])
                    
                    
        if len(self.button_coords) ==1:
            butn_items = ['top', 'bot']
            butn = dict(zip(butn_items, [self.button_coords[0][1], self.button_coords[0][2]]))
            final_list.append(['Button', butn])
        elif len(self.button_coords) >= 1:
            for b_idx, button in enumerate(self.button_coords[:-1]):
                if button[1][1] > self.button_coords[idx+1][1][1]:
                    butn_items = ['top', 'bot']
                    butn = dict(zip(butn_items, [button[1], button[2]]))
                    final_list.append(['Button', butn])
                else:
                    butn_items = ['top', 'bot']
                    butn = dict(zip(butn_items, [self.button_coords[idx+1][1], self.button_coords[idx+1][2]]))
                    final_list.append(['Button', butn])
        os.chdir(self.PATH)
        
        crop = [img for img in os.listdir('cropper') if img.endswith('.jpg')]
        for nb, c in enumerate(crop):
            os.remove(os.path.join(os.path.abspath('cropper'), c))
    
        with open('classified.json', 'w') as f:
            json.dump(final_list, f)

        dic = {}
        one_more_time = open('classified.json', 'r')
        data = json.load(one_more_time)
        for c,d in enumerate(data):
            dic[d[0]] = d[1]
            
        self.dic = dic
        # Draw field classes 
        os.chdir('images')
        for keys in dic:
            final_img = cv2.imread(self.image_path)
            font = cv2.FONT_HERSHEY_SIMPLEX
            lineThickness = 2
            cv2.rectangle(final_img, (int(dic[keys]['top'][0]), int(dic[keys]['top'][1])), (int(dic[keys]['bot'][0]), int(dic[keys]['bot'][1])), (255,0,255), lineThickness)
            cv2.putText(final_img, keys, (int(dic[keys]['bot'][0]), int(dic[keys]['bot'][1])), font, 1,(255,0,255),2,cv2.LINE_AA)
            cv2.imwrite('zzz' + str(keys) + '.jpg', final_img)
        
        os.chdir(self.PATH)
        
        if not os.path.exists('date'):
            os.mkdir('date')
        else:
            None
        os.chdir('date')
        
        # Crop date fields to discover data input format
        for itr, keys in enumerate(dic):
            if keys == 'Date' and dic[keys]['field_type'] == 'input':
                date_img = cv2.imread(self.image_path)
                date_img = date_img[int(dic[keys]['top'][1])+10:int(dic[keys]['bot'][1]), int(dic[keys]['top'][0])-5:int(dic[keys]['bot'][0])]
                cv2.imwrite('cropped_date' + str(itr) + '.jpg', date_img)
            elif keys == 'Year' and dic[keys]['field_type'] == 'input' or keys == 'Month' and dic[keys]['field_type'] == 'input':
                date_img = cv2.imread(self.image_path)
                date_img = date_img[int(dic[keys]['top'][1])+10:int(dic[keys]['bot'][1]), int(dic[keys]['top'][0])-5:int(dic[keys]['bot'][0])]
                cv2.imwrite('cropped_date' + str(itr) + '.jpg', date_img)
                
        date_imgs_path = os.getcwd()
        date_images = [img for img in os.listdir(date_imgs_path) if img.endswith('.jpg')]
        for di in date_images:
            data_format = ts.image_to_string(di, lang='eng+ukr')
            print(data_format)
                
        os.chdir(self.PATH)
        # Write final JSON file
        with open('classified.json', 'w') as f:
            json.dump(dic, f)    
            
        #self.__visualizer()
        
        images = [img for img in os.listdir('images') if img.endswith('.jpg')]
        l = len(images)
        for i, image in enumerate(images):
            os.remove(os.path.join(os.path.abspath('images'), image))