# Deep UI 
**Instalation (via terminal)**
- brew install python3
- brew install tesseract
- git clone https://gitlab.com/mynameisJura/pioneer.git
- cd pioneer
- python3 -m venv env
- source env/bin/activate
- pip3 install -r requirements.txt

**Requirements**

You need Firefox browser installed.

You need to allow Terminal to control your computer at Accessibility tab on Security & Privacy settings on the System Preferences.

**Run this command to start programm**

```python3 login_demo.py --login="YOUR_USERNAME_OR_EMAIL" --password="YOUR_PASSWORD" --url="YOUR_URL"```

*Example:*

```python3 login_demo.py --login="email@deep.ui" --password="Typical_password_123" --url="https://play.pioneer.app/login"```


## Video Instruction

[![](http://img.youtube.com/vi/VuFmYhJJPu8/0.jpg)](http://www.youtube.com/watch?v=VuFmYhJJPu8 "")

Click image to open video on YouTube

