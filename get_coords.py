import numpy as np
from skimage.transform import resize
import torch
from torch.autograd import Variable
import datetime
from models import Darknet
import cv2
import logging
from utils.utils import load_classes, non_max_suppression
import time
import base64
import torch.nn.functional as F

import json

class Predictor:
    def __init__(self, 
                 class_path='/home/ubuntu/darknet/deepUI/classes.names', 
                 config_path='/home/ubuntu/darknet/deepUI/yolov3.cfg',
                 weights_path='weights/yolov3_11000.weights'):
        self.image_size = 608
        self.class_path = class_path
        self.config_path = config_path
        self.weights_path = weights_path
        self.use_cuda = False
        self.cuda = torch.cuda.is_available()
        
            
        # Set up model
        self.model = Darknet(self.config_path, img_size=self.image_size)
        self.model.load_darknet_weights(self.weights_path)
        self.model.share_memory()
        self.model.eval() # Set in evaluation mode

        self.classes = load_classes(self.class_path) # Extracts class labels from file
        self.Tensor = torch.cuda.FloatTensor if self.cuda else torch.FloatTensor

        if self.cuda:
            self.model.cuda()

    
    def __resize_img(self, img):
        try:
            img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
            # Extract image height and weight
            h, w, _ = img.shape
            img_shape = (self.image_size, self.image_size)
            dim_diff = np.abs(h - w)
            # Upper (left) and lower (right) padding
            pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
            # Determine padding
            pad = ((pad1, pad2), (0, 0), (0, 0)) if h <= w else ((0, 0), (pad1, pad2), (0, 0))
            # Add padding
            input_img = np.pad(img, pad, 'constant', constant_values=127.5) / 255
            # Resize and normalize
            input_img = resize(input_img, (*img_shape, 3), mode='reflect') 
            # Channels-first
            input_img = np.transpose(input_img, (2, 0, 1))
            # As pytorch tensor
            input_img = torch.from_numpy(input_img).float().unsqueeze(0)
            return input_img
        except:
            logging.exception('Got exception on PREDICTOR handler')
            raise
            
    
        
    def draw_box(self, img, i, detections):
        boxes = []
        labels = []
        inputs = []
        buttons = []
        dropboxes = []
        predicted_coords = []
        elements_list = ['labels', 'inputs', 'dropboxes', 'buttons']
        dict_data = ['x1', 'y1', 'x2', 'y2', 'center']
        # The amount of padding that was added
        pad_x = max(img[i].shape[0] - img[i].shape[1], 0) * (self.image_size / max(img[i].shape))
        pad_y = max(img[i].shape[1] - img[i].shape[0], 0) * (self.image_size / max(img[i].shape))
        # Image height and width after padding is removed
        unpad_h = self.image_size - pad_y
        unpad_w = self.image_size - pad_x
        for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
            # Rescale coordinates to original dimensions
            box_h = ((y2 - y1) / unpad_h) * img[i].shape[0]
            box_w = ((x2 - x1) / unpad_w) * img[i].shape[1]
            y1 = ((y1 - pad_y // 2) / unpad_h) * img[i].shape[0]
            x1 = ((x1 - pad_x // 2) / unpad_w) * img[i].shape[1]
            y2 = ((y2 - pad_y // 2) / unpad_h) * img[i].shape[0]
            x2 = ((x2 - pad_x // 2) / unpad_w) * img[i].shape[1]
            y_center = y2-((y2-y1)/2)
            x_center = x2-((x2-x1)/2)
            box = [x1.item(), abs(y1.item()), x2.item(), y2.item(), [x_center.item(), y_center.item()]]
            boxes.append(box)
            predicted_class = self.classes[int(cls_pred)]
            print(predicted_class)
            if predicted_class == 'input':
                inputs.append(dict(zip(dict_data, box)))
                inputs.sort(key=lambda x: x['center'][1])
            elif predicted_class == 'label':
                labels.append(dict(zip(dict_data, box)))
                labels.sort(key=lambda x: x['center'][1])
            elif predicted_class == 'button':
                buttons.append(dict(zip(dict_data, box)))
                buttons.sort(key=lambda x: x['center'][1])
            elif predicted_class == 'dropbox':
                dropboxes.append(dict(zip(dict_data, box)))
                dropboxes.sort(key=lambda x: x['center'][1])
            score = cls_conf.item()
            label = '%s: %s' % (predicted_class, round(score * 100, 2)) + '%'
            # Draw a bounding box.
            cv2.rectangle(img[i], (x1, y1), (x2, y2), (255, 178, 50), 3)
            #Display the label at the top of the bounding box
            labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
            y1 = max(y1, labelSize[1])
            cv2.rectangle(img[i], (x1, y1 - round(1.5*labelSize[1])), (x1 + round(1.5*labelSize[0]), y1 + baseLine), (255, 255, 255), cv2.FILLED)
            cv2.putText(img[i], label, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,0), 1)
            cv2.imwrite('res.jpg', img[i])
        # Write coordinates to jsons (only if particular class was found)
        predicted_coords.append(dict(zip(elements_list, [labels, inputs, dropboxes, buttons])))
        with open('predicted_coords_test.json', 'w') as pc:
            json.dump(predicted_coords, pc)

        return boxes
    
    def predict(self, image):
        try:
            start_time = time.time()
            img = []
            batch_size = 2
            leftover = 0
            inp_dim = 608
            img = image.copy()
            
            im_batches = list(map(self.__resize_img, [image]))
            if (len(image) % batch_size):
                leftover = 1
            if batch_size != 1:
                num_batches = 2 // batch_size + leftover
                im_batches = [torch.cat((im_batches[i*batch_size : min((i +  1)*batch_size,
                                   len(im_batches))])) for i in range(num_batches)]  
                
            i = 0
            for j,batch in enumerate(im_batches):
                img_detections = []
                if self.cuda:
                    batch = batch.cuda()
                prev_time = time.time()
                input_img = Variable(batch.type(self.Tensor))
                # Get detections
                with torch.no_grad():
                    detections = self.model(input_img)
                    detections = non_max_suppression(detections, 0.2, 0.5)
                # Log progress
                current_time = time.time()
                inference_time = datetime.timedelta(seconds=current_time - prev_time)
                prev_time = current_time
                img_detections.extend(detections)
                for detections in img_detections:
                    if detections is not None:
                        unique_labels = detections[:, -1].cpu().unique()
                        n_cls_preds = len(unique_labels)
                        
                        #draw detections
                        box = self.draw_box([img, img], i, detections)
                    i = i + 1
            end_time = time.time()
            inference_time = datetime.timedelta(seconds=end_time - start_time)
            return image
        except:
            logging.exception('Got exception on PREDICTOR handler')
            raise