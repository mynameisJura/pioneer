import argparse
import json
import os
import time

import cv2

from simulator import Simulator
from get_coords import Predictor
from merge_coord import Mapping

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--driver", type=str, required=False,
                    help="path to driver",
                    default="./geckodriver")
parser.add_argument("-u", "--url", type=str, required=True,
                    help="url of form page")

parser.add_argument("-n", "--login", type=str, required=True,
                    help="security code on card")
parser.add_argument("-p", "--password", type=str, required=True,
                    help="month on card")

args = parser.parse_args()

arg_field_map = {
    "Username": args.login,
    "Password": args.password,
}

def main():

    sim = Simulator(driver=args.driver, url=args.url)
    time.sleep(3)
    screenshot_path = sim.screenshot()

    pr = Predictor(class_path="./classes.names",
                   config_path="yolov3.cfg",
                   weights_path="yolov3_11000.weights")
    img = cv2.imread(screenshot_path)
    res = pr.predict(img)

    p = Mapping(os.path.realpath("predicted_coords_test.json"),
                os.path.realpath("screenshot.png"),
                os.path.realpath("New_case.txt"))
    p.classifier()

    print(json.dumps(p.dic, sort_keys=True, indent=4))

    # go by fields and fill it with provided information
    for key in p.dic:
        if key != "Button":
            dest_x = int((p.dic[key]["top"][0]+p.dic[key]["bot"][0])/2/2)
            dest_y = int((p.dic[key]["top"][1]+p.dic[key]["bot"][1])/2/2)

            text = arg_field_map[key]

            sim.move(dest_x, dest_y)
            sim.click()
            sim.write(text)

    # click on finish button
    if "Button" in p.dic:
        dest_x = int((p.dic["Button"]["top"][0]+p.dic["Button"]["bot"][0])/2/2)
        dest_y = int((p.dic["Button"]["top"][1]+p.dic["Button"]["bot"][1])/2/2)

        sim.move(dest_x, dest_y)
        sim.click()


if __name__ == "__main__":
    main()